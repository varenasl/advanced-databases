## Diseño y operaciones CRUD en Bases de datos NoSQL
--

Para esta actividad se ha tomado como referencia el sistema de competencia de la liga de fútbol colombiano “BetPlay DIMAYOR II 2022”. El diseño de la base de datos que se va a proponer esta basado en las reglas establecidas por la DIMAYOR para la realización de este evento.  

Tomando como referencia la bibliografía propuesta para esta actividad, se ha decidió diseñar un modelo de bases de datos no relacional, con el que se pretende realizar toda la gestión del campeonato, sus fases y sus participantes, aprovechando las ventajas que ofrecen este tipo de bases de datos. 

Dicha base de datos constará de cinco (5) colecciones donde se almacenarán los documentos correspondientes a cada entidad modelada para este tipo de torneos. Se proponen las colecciones de arbitro, entrenador, equipo, jugador y resultado. Cada una tendrá almacenada información de una forma no estructurada con el objetivo de tener más flexibilidad y rendimiento en las operaciones de lectura y escritura. 

### Reglamento de la competencia
--

1. En cada uno de los partidos de dicha competencia se adjudicarán 3 puntos por partido ganado, 1 punto por partido empatado y 0 puntos por partido perdido. 

2. Las fases que se definan por tiros penales no otorgarán puntos adicionales a los generados durante el desarrollo del partido y tampoco sumarán los goles obtenidos en la tabla de goleadores. 

3. La competencia se realizará con la participación de 10 equipos de fútbol. 

4. Los clubes que ocupen los 4 primeros lugares en la tabla de posiciones finalizada la fase 1, serán los clasificados a los cuadrangulares semifinales de la competencia. 

5. El club que obtenga mayor puntuación en los partidos de ida y vuelta en la final de la competencia será el campeón. 

Siguiendo con la normatividad, se toman como referencia las [reglas generales del fútbol](https://es.wikipedia.org/wiki/Reglas_del_f%C3%BAtbol) conocidas abiertamente por los participantes del torneo.